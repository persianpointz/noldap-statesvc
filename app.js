/*********************************************************************************
Dependencies
**********************************************************************************/
var cluster     = require('cluster');
var numCPUs     = require('os').cpus().length;
var restify 	= require('restify');
var config 		= require('config');
var uuid 		= require('uuid');
var repository  = require('./dataRepositories/cb');
/*********************************************************************************/

/**********************************************************************************
Configuration
**********************************************************************************/
var appInfo 		= require('./package.json');
var port 			= process.env.PORT || 1337;
var server 			= restify.createServer();
/*********************************************************************************/

/**********************************************************************************
Constants
**********************************************************************************/
var headerXSessionToken             = 'x-session-token';
var headerKey                       = 'x-key';
/*********************************************************************************/

/**********************************************************************************
Setup
**********************************************************************************/
if (cluster.isMaster) {

    console.log('numCPUs: ' + numCPUs);
    
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on('exit', function(worker, code, signal) {
        console.log('worker ' + worker.process.pid + ' died');
    });

} else {
    server.use(restify.queryParser());
    server.use(restify.bodyParser());
    server.use(restify.CORS());
    server.opts(/.*/, function (req,res,next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", req.header("Access-Control-Request-Method"));
        res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
        res.send(200);
        return next();
    });
    server.use(restify.gzipResponse());

    /**********************************************************************************
    End-points
    **********************************************************************************/
    //Echo
    server.get({path: '/echo', flags: 'i'}, echo);

    function echo(req, res, next) {
        var info = {
            name: appInfo.name,
            version: appInfo.version,
            description: appInfo.description,
            author: {
                name: appInfo.author.name,
                email: appInfo.author.email,
            }
        };
        res.send(info);
        next();
    }    

    //Create Session validated by x-key
    server.post({path: '/', flags: 'i'}, function (req, res, next) {

        var input = parseRequest(req);

        validateKey(
            req, 
            function(){
                var session = input;
                
                session.Id = uuid.v4();
                setExpiryDate(session);
              
                repository.updateItem(
                    session,
                    config.sessionExpirySec,
                    function(session){
                        res.statusCode = 201;
                        res.send(session);
                    },
                    onError
                );
            },
            function(){
                return next(new restify.errors.UnauthorizedError('Invalid key'));
            });
    });
    
    //Get Session validated by x-session-token
    server.get({path: '/', flags: 'i'}, function (req, res, next) {

        getSession(
            req,
            next,
            function(session) {
                setExpiryDate(session);
                repository.updateItem(
                    session, 
                    config.sessionExpirySec,
                    function(session){
                        res.send(session);        
                    },
                    onError
                );
            },
            onError
        );
    });
    
    //Update Session validate by-x-session-token
    server.put({path: '/', flags: 'i'}, function (req, res, next) {

        var input = parseRequest(req);

        getSession(
            req,
            next,
            function(session) {
                input.Id = session.Id;
                session = input;
                setExpiryDate(session);
                repository.updateItem(
                    session,
                    config.sessionExpirySec,
                    function(session){
                        res.send(session);        
                    },
                    onError
                );
            },
            onError
        );
    });
    
    //Validate session by x-session-token
    server.get({path: '/validate', flags: 'i'}, function (req, res, next) {

        getSession(
            req,
            next,
            function(session) {
                setExpiryDate(session);
                repository.updateItem(
                    session, 
                    config.sessionExpirySec,
                    function(session){
                        res.send({
                            IsValid: true,
                            Expires: session.Expires
                        });        
                    },
                    onError
                );
            },
            onError
        );
    });
    
    //Delete Session validated by x-session-token
    server.del({path: '/', flags: 'i'}, function (req, res, next) {

        var sessionToken = getSessionToken(req);
        repository.deleteItem(
            sessionToken, 
            function(session) {
                res.send({ Deleted: true});        
            },
            function(session) {
                res.statusCode = 404;
                res.send({ Deleted: false});        
            },
            onError
        );
    });
    
    //Path Session validate by-x-session-token
    server.patch({path: '/', flags: 'i'}, function (req, res, next) {

        var input = parseRequest(req);

        getSession(
            req,
            next,
            function(session) {
                for (var key in input) {
                    session[key] = input[key];    
                }
                setExpiryDate(session);
                repository.updateItem(
                    session,
                    config.sessionExpirySec,
                    function(session){
                        res.send(session);        
                    },
                    onError
                );
            },
            onError
        );
    });
    /********************************************************************************/

    /*********************************************************************************
    Utilities
    *********************************************************************************/
    function validateKey(req, onSuccess, onError) {
        
        var key = req.headers[headerKey];
        if (key == null || key.length <= 0)
            throw (new restify.errors.BadRequestError('Missing x-key header'));

       if (key == config.credentials.key) {
            onSuccess();
        } else {
            onError();
        }
    }
    
    function setExpiryDate(session){
        var now = new Date();
        var expiryDate = new Date(now.setSeconds(now.getSeconds() + config.sessionExpirySec));
        session.Expires = expiryDate;
    }
    
    function onError (err) {
        console.error(err);
        var msg = (err != null && err.message != null)? err.message: 'Unknown error';
        throw (new restify.errors.InternalServerError(msg));
    };    

    function parseRequest(req) {
        var output = {};
        if (typeof req.body == 'string') {
            output = JSON.parse(req.body);
        } else {
            output = req.body || {};
        }
        return output;
    }
    
    function getSessionToken(req, onSuccess) {
        
        var sessionToken = req.headers[headerXSessionToken];
        if (sessionToken == null || sessionToken.length == 0)
            throw new restify.errors.UnauthorizedError('x-session-token header not found');
        return sessionToken;
    }
    
    function getSession(req, next, onSuccess, onError) {
        
        var sessionToken = getSessionToken(req);
        
        var msgNotFound = 'Session with token:\'{sessionToken}\' was not found'
                            .replace('{sessionToken}', sessionToken);

        var onNotFound = function(){
            return next(new restify.errors.UnauthorizedError(msgNotFound));
        };
        
        repository.getItem(
            sessionToken,
            function(session){
                if (session.Expires <= new Date()) {
                    onNotFound();
                } else {
                    onSuccess(session);
                }
            },
            onNotFound,
            onError
        );
    }
    /*********************************************************************************/

    /**********************************************************************************
    Start the server
    **********************************************************************************/
    server.listen(port, function() {
    });
    /*********************************************************************************/
}