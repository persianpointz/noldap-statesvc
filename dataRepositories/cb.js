/*********************************************************************************
Dependencies
**********************************************************************************/
var config 		= require('config');
var couchbase 	= require('couchbase');
/*********************************************************************************/

/**********************************************************************************
Config
**********************************************************************************/
var cbCluster   = new couchbase.Cluster(config.couchbase.host);
var cbBucket    = cbCluster.openBucket(config.couchbase.bucket, config.couchbase.password); 
/*********************************************************************************/

/**********************************************************************************
Module
**********************************************************************************/
exports.addItem =  function (session, expiryInSec, onSuccess, onError) {
    upsertSession(session, expiryInSec, onSuccess, onError);
};

exports.getItem = function(id, onSuccess, onError) {
    
    cbBucket.get(
        id, 
        {},
        function(err, response) {
            if (err) {
                onError(err);
            } else {
                var session = deleteJsonType(response.value);
                onSuccess(session);
            }
    }); 
};

exports.updateItem = function(session, expiryInSec, onSuccess, onError) {
    upsertSession(session, expiryInSec, onSuccess, onError);
};


exports.deleteItem = function(id, onSuccess, onError) {
    
    cbBucket.remove(
        id, 
        function(err, response) {
            if (err) {
                onError(err);
            } else {
                onSuccess();
            }
    }); 
};
/*********************************************************************************/
function upsertSession(session, expiryInSec, onSuccess, onError) {

    session = addJsonType(session, config.couchbase.jsonTypeSession);
    
    cbBucket.upsert(
        session.Id, 
        session,
        { expiry: expiryInSec },
        function(err, response){
            if (err) {
                onError(err);
            } else {
                session = deleteJsonType(session);
                onSuccess(session);
            }
        }
    );
}

function addJsonType(doc, jsonType) {
    doc.JsonType = jsonType;
    return doc;
}

function deleteJsonType(doc) {
    delete doc["JsonType"];
    return doc;
}
