/*********************************************************************************
Dependencies
**********************************************************************************/
var config          = require('config');
var redis           = require("redis");
/*********************************************************************************/

/**********************************************************************************
Utilities
**********************************************************************************/
var client = null;

function connect2Redis(onSuccess, onError) {
    
    client = redis.createClient(
        config.redis.port, 
        config.redis.host, 
        {
            auth_pass: config.redis.accessKey,
            detect_buffers: true
        });

    client.on('connect', function() {
        console.log('connection to redis established');
    });

    client.on('error', function (err) {
        onError(err);
    });
}

function executeRedis(onSuccess, onError) {

    if (client != null && client.connected)
        onSuccess();
    else {
        connect2Redis(onSuccess, onError);
        onSuccess();
    }
        
}
/*********************************************************************************/

/**********************************************************************************
Module
**********************************************************************************/
exports.addItem =  function (session, expiryInSec, onSuccess, onError) {

    var value = JSON.stringify(session);
    executeRedis(
        function(){
            client.set(session.Id, value, function(err, reply) {
                if (err) {
                    onError(err);
                } else {
                    client.expire(session.Id, expiryInSec, function(errExpire, replyExpire) {
                        if (errExpire) {
                            onError(errExpire);
                        } else {
                            onSuccess(session);
                        }
                        
                    });
                }
            });
        },
        onError
    );
};

exports.getItem = function(id, onSuccess, onNotFound, onError) {
    executeRedis(
        function(){
            client.get(id, function(err, reply) {
                if (err)
                    onError(err);
                else if (reply) {
                    var session = JSON.parse(reply);
                    onSuccess(session);
                } else  {
                    onNotFound();
                }
            });
        },
        onError
    );
};

exports.updateItem = function(session, expiryInSec, onSuccess, onError) {
    this.addItem(session, expiryInSec, onSuccess, onError);
};

exports.deleteItem = function(sessionId, onSuccess, onNotFound, onError) {
    executeRedis(
        function(){
            client.del(sessionId, function(err, reply) {
                if (err)
                    onError(err);
                else if (reply == 1)
                    onSuccess();
                else
                    onNotFound();
            });
        },
        onError
    );
};
/*********************************************************************************/