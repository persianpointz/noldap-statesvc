var config = require('../config/default.json');
var redis = require("redis");
var client = null;

function connect() {
    
    var client = redis.createClient(
        config.redis.port, 
        config.redis.host, 
        {
            auth_pass: config.redis.accessKey,
            detect_buffers: true
        });

    
    client.on('connect', function() {
        console.log('connected');

        client.set('framework', JSON.stringify({name:"AngularJS-no expiry"}), function(err, reply) {
            if (err)
                console.err(err);
            else
                console.log(reply);
        });

        client.get('framework', function(err, reply) {
            if (err)
                console.err(err);
            else
                console.log(JSON.parse(reply).name);
        });
/*        
        client.set('framework', JSON.stringify({name:"AngularJS"}), function(err, reply) {
            if (err)
                console.err(err);
            else
                console.log(reply);
        });
        
        client.expire('framework', 1);        

        var myTimer = setInterval(function() {
                client.get('framework', function (err, reply) {
                    if(reply) {
                        console.log('I live: ' + reply.toString());
                    } else {
                        clearTimeout(myTimer);
                        console.log('I expired');
                        client.quit();
                    }
                });
            }, 500);        

        client.set('framework', JSON.stringify({name:"AngularJS-no expiry"}), function(err, reply) {
            if (err)
                console.err(err);
            else
                console.log(reply);
        });
*/
        client.del('framework', function(err, reply) {
            if (err)
                console.err(err);
            else
                console.log(reply);
        });

        client.del('framework', function(err, reply) {
            if (err)
                console.err(err);
            else
                console.log(reply);
        });

//        client.get('framework', function(err, reply) {
//            if (err)
//                console.err(err);
//            else if (reply)
//                console.log(JSON.parse(reply).name);
//            else
//                console.log('not found');
//        });

    });

    client.on('error', function (err) {
        console.error(err);
    });
}

connect();
